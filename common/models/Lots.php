<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lots".
 *
 * @property integer $id
 * @property integer $lot_category_id
 * @property integer $price
 * @property string $description
 * @property string $description_full
 *
 * @property LotCategories $lotCategory
 */
class Lots extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lots';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lot_category_id', 'price'], 'required'],
            [['lot_category_id', 'price'], 'integer'],
            [['description', 'description_full'], 'string'],
            [['lot_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => LotCategories::className(), 'targetAttribute' => ['lot_category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lot_category_id' => Yii::t('app', 'Lot Category ID'),
            'price' => Yii::t('app', 'Price'),
            'description' => Yii::t('app', 'Description'),
            'description_full' => Yii::t('app', 'Description Full'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLotCategory()
    {
        return $this->hasOne(LotCategories::className(), ['id' => 'lot_category_id']);
    }

    /**
     * @inheritdoc
     * @return LotsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LotsQuery(get_called_class());
    }
}
