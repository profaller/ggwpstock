<?php

namespace common\models;

use profaller\behaviors\UploadFilesBehavior;
use Yii;

/**
 * This is the model class for table "games".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property string $banner_image
 * @property string $bg_image
 * @property string $description
 *
 * @property LotCategories[] $lotCategories
 */
class Games extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'games';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadFilesBehavior::className(),
                'uploadPath' => Yii::getAlias('@frontendweb') . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'games' . DIRECTORY_SEPARATOR,
                'uploadUrl' => '/uploads/games/',
                'attributes' => ['image', 'banner_image', 'bg_image'],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['image'], 'filesValidator', 'skipOnEmpty' => false],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string'],
        ];
    }

    public function filesValidator($attribute, $params)
    {
        $this->instantiateFile();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'image' => Yii::t('app', 'Image'),
            'banner_image' => Yii::t('app', 'Banner Image'),
            'bg_image' => Yii::t('app', 'Bg Image'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    public function getLotCategories()
    {
        return $this->hasMany(LotCategories::className(), ['game_id' => 'id']);
    }
}
