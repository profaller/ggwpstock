<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[LotCategories]].
 *
 * @see LotCategories
 */
class LotCategoriesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return LotCategories[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return LotCategories|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
