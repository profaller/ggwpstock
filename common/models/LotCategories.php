<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lot_categories".
 *
 * @property integer $id
 * @property integer $game_id
 * @property integer $type
 * @property string $name
 * @property integer $order
 * @property string $description
 *
 * @property Games $game
 * @property Lots[] $lots
 */
class LotCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lot_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'type', 'name'], 'required'],
            [['game_id', 'type', 'order'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['game_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'game_id' => Yii::t('app', 'Game ID'),
            'type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Name'),
            'order' => Yii::t('app', 'Order'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Games::className(), ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLots()
    {
        return $this->hasMany(Lots::className(), ['lot_category_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return LotCategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LotCategoriesQuery(get_called_class());
    }
}
