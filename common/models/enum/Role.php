<?php

namespace common\models\enum;


use common\components\Enum;

class Role extends Enum
{
    const ADMIN        = 'admin';
    const END_USER     = 'end_user';


    public static function getNamesList()
    {
        return [
            self::ADMIN    => \Yii::t('app', 'Admin'),
            self::END_USER => \Yii::t('app', 'End User'),
        ];
    }

    public static function getList()
    {
        return [
            self::ADMIN,
            self::END_USER,
        ];
    }


}