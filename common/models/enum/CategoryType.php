<?php

namespace common\models\enum;


use common\components\Enum;

class CategoryType extends Enum
{
    const MONEY         = 1;
    const ACCOUNTS      = 2;
    const SERVICES      = 3;
    const THINGS        = 4;


    public static function getNamesList()
    {
        return [
            self::MONEY    => \Yii::t('app', 'Money'),
            self::ACCOUNTS => \Yii::t('app', 'Accounts'),
            self::SERVICES => \Yii::t('app', 'Services'),
            self::THINGS => \Yii::t('app', 'things'),
        ];
    }

    public static function getList()
    {
        return [
            self::MONEY,
            self::ACCOUNTS,
            self::SERVICES,
            self::THINGS,
        ];
    }


}