<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Lots]].
 *
 * @see Lots
 */
class LotsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Lots[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Lots|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
