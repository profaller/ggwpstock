(function($) {

    App.Ctrl.Games = {};

    App.Ctrl.Games.Categories = can.Control.extend(
        {
            pluginName: 'gamesCategories'
        },
        {
            init: function () {
                this.newCount = 0;
            },

            '.js-add-proto click': function (btn, e) {
                var newBlock = this.element.find('.js-proto').clone();


                newBlock.find('.js-control').each(this.proxy(function (i, el) {
                    var control = $(el),
                        name = control.attr('name');

                    control.attr('name', 'newAttribute['+ this.newCount +']['+ name +']')
                }));

                this.newCount++;

                newBlock.show().removeClass('js-proto').insertBefore(btn);
            },

            '.js-delete click': function (el, e) {
                e.preventDefault();

                $.ajax({
                    url: el.attr('href'),
                    type: 'POST'
                });

                el.closest('.form-group').remove();
            }

        }
    );


})(jQuery);
