<?php
function dbg($v) {
    while (@ob_end_clean()) {}
    echo '<pre>' . print_r($v, true) . '</pre>';
    die();
}

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'assetManager' => [
            'linkAssets' => true,
        ],
    ],
];
