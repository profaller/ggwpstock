'use strict';

var fs = require('fs');
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require('gulp-rename');

function watch (type) {
    return function () {
        gulp.watch(
            ['./common/assets/src/scss/frontend/*.scss'],
            ['build_styles_frontend']
        );

        gulp.watch(
            ['./common/assets/src/scss/backend/*.scss'],
            ['build_styles_backend']
        );

        gulp.watch(
            ['./common/assets/src/js/backend/*.js'],
            ['build_js_backend']
        );

        gulp.watch(
            ['./common/assets/src/js/frontend/*.js'],
            ['build_js_backend']
        );
    };
}

function build_styles_frontend () {
    return function () {
        gulp.src(['./common/assets/src/scss/frontend/frontend.scss'])
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest('./common/assets/dest/styles/'));
    };
}

function build_js_frontend () {
    return function () {
        gulp.src(['./common/assets/src/js/frontend/app.js'])
            .pipe(rename('app_frontend.js'))
            .pipe(gulp.dest('./common/assets/dest/js/'));
    };
}



function build_styles_backend () {
    return function () {
        gulp.src(['./common/assets/src/scss/backend/backend.scss'])
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest('./common/assets/dest/styles/'));
    };
}

function build_js_backend () {
    return function () {
        gulp.src([
            './common/assets/src/js/vendor/can.custom.js',
            './common/assets/src/js/core/app.main.js',
            './common/assets/src/js/core/app.common.js',
            './common/assets/src/js/backend/app.games.js'])
            .pipe(concat('app_backend.js'))
            .pipe(gulp.dest('./common/assets/dest/js/'));
    };
}

function createTasks (type) {
    gulp.task('watch', watch());
    gulp.task('build_styles_frontend', build_styles_frontend());
    gulp.task('build_styles_backend', build_styles_backend());
    gulp.task('build_js_frontend', build_js_frontend());
    gulp.task('build_js_backend', build_js_backend());
    gulp.task(type, ['build_styles_frontend', 'build_styles_backend', 'build_js_backend', 'build_js_frontend', 'watch'])
}

createTasks('default');
