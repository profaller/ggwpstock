<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Games */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="games-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>




    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if ($model->image) { ?>
        <?= Html::img($model->getUploadUrl('image'), ['class' => 'img-preview'])?>
    <?php } ?>

    <?= $form->field($model, 'image')->fileInput() ?>

    <hr>

    <?php if ($model->bg_image) { ?>
        <?= Html::img($model->getUploadUrl('bg_image'), ['class' => 'img-preview'])?>
    <?php } ?>

    <?= $form->field($model, 'bg_image')->fileInput() ?>

    <hr>

    <?php if ($model->banner_image) { ?>
        <?= Html::img($model->getUploadUrl('banner_image'), ['class' => 'img-preview']) ?>
    <?php } ?>

    <?= $form->field($model, 'banner_image')->fileInput() ?>

    <hr>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'active')->checkbox() ?>


    <h2>Categories</h2>
    <hr>

    <?= $this->render('_categories', compact('model'))?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
