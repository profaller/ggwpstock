<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Games */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Games',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Games'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="games-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
