<?php


/* @var $this yii\web\View */
use common\models\enum\CategoryType;
use common\models\LotCategories;
use yii\helpers\Html;

/* @var $model common\models\Games */
/* @var $form yii\widgets\ActiveForm */

$categoryModel = new LotCategories();
?>

<div data-app-controller="gamesCategories">

<div class="row form-group payment-method-catalog-form__header">
    <div class="col-md-2">
        <?= $categoryModel->getAttributeLabel('name')?>
    </div>
    <div class="col-md-4">
        <?= $categoryModel->getAttributeLabel('type')?>
    </div>
    <div class="col-md-2">
        <?= $categoryModel->getAttributeLabel('order')?>
    </div>
    <div class="col-md-2">
        <?= $categoryModel->getAttributeLabel('description')?>
    </div>
    <div class="col-md-2">

    </div>
</div>

<?php foreach ($model->lotCategories as $i => $category) {?>

    <div class="row form-group">
        <?= Html::activeHiddenInput($category, '['.$i.']game_id', ['class' => 'form-control', 'value' => $model->id])?>
        <div class="col-md-4">
            <?= Html::activeTextInput($category, '['.$i.']name', ['class' => 'form-control'])?>
        </div>
        <div class="col-md-4">
            <?= Html::activeDropDownList($category, '['.$i.']type', CategoryType::getNamesList(), ['class' => 'form-control'])?>
        </div>
        <div class="col-md-2">
            <?= Html::activeTextInput($category, '['.$i.']description', ['class' => 'form-control', 'label' => false])?>
        </div>
        <div class="col-md-2">
            <?= Html::a('Del', ['delete-category', 'id' => $category->id], ['class' => 'btn btn-danger js-delete'])?>
        </div>
    </div>

<?php } ?>


<div class="row form-group js-proto" style="display: none">
    <div class="col-md-4">
        <?= Html::textInput('name', null, ['class' => 'form-control js-control'])?>
    </div>
    <div class="col-md-4">
        <?= Html::dropDownList('type', null, CategoryType::getNamesList(), ['class' => 'form-control js-control'])?>
    </div>
    <div class="col-md-4">
        <?= Html::textInput('description', null, ['class' => 'form-control js-control'])?>
    </div>
</div>

<div class="form-group clearfix">
    <a class="btn btn-default pull-left js-add-proto">Добавить</a>
</div>

</div>
