<?php

use yii\db\Migration;

class m161215_014927_tbl_game extends Migration
{
    public function up()
    {
        $this->createTable('games', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image' => $this->string(),
            'banner_image' => $this->string(),
            'bg_image' => $this->string(),
            'description' => $this->text(),
            'active' => $this->boolean()->defaultValue(true),
        ]);
    }

    public function down()
    {
        echo "m161215_014927_tbl_game cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
