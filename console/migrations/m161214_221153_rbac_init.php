<?php

use common\models\enum\Role;
use common\models\Users;
use yii\db\Migration;

class m161214_221153_rbac_init extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $role = $auth->createRole(Role::ADMIN);
        $auth->add($role);

        $role = $auth->createRole(Role::END_USER);
        $auth->add($role);

        $model = new Users();
        $model->username = 'admin';
        $model->password = 'ghbrjk';
        $model->role = Role::ADMIN;
        $model->save();
    }

    public function down()
    {
        echo "m161214_221153_rbac_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
