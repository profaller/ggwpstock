<?php

use yii\db\Migration;

class m161216_042946_tbl_game_lot_types extends Migration
{
    public function up()
    {
        $this->createTable('lot_categories', [
            'id' => $this->primaryKey(),
            'game_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'order' => $this->integer()->notNull()->defaultValue(500),
            'description' => $this->text(),
        ]);

        $this->createTable('lots', [
            'id' => $this->primaryKey(),
            'lot_category_id' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'description' => $this->text(),
            'description_full' => $this->text(),
        ]);

        $this->addForeignKey('games_to_lot_categories_ref', 'lot_categories', 'game_id', 'games', 'id', 'CASCADE');
        $this->addForeignKey('lot_category_to_lots_ref', 'lots', 'lot_category_id', 'lot_categories', 'id');
    }

    public function down()
    {
        echo "m161216_042946_tbl_game_lot_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
